def coin(money):
    change_stat = dict()
    coins = [1, 2, 4, 5, 10]
    for i in reversed(sorted(coins)):
        change_stat[i] = money // i
        money = money % i
    return change_stat


def change_coin(money):
    change_stat = dict()
    coins = [1, 2, 5, 10]
    for i in reversed(sorted(coins)):
        change_stat[i] = money // i
        money = money % i
    return change_stat


def dyna_coin(money):
    x10 = money // 10
    money %= 10
    x5 = money // 5
    money %= 5
    x4 = money // 4
    money %= 4
    x2 = money // 2
    x1 = money % 2
    return "10: {}, 5: {}, 4: {}, 2: {}, 1: {}".format(x10, x5, x4, x2, x1)


def change_dyna_coin(money):
    x10 = money // 10
    money %= 10
    x5 = money // 5
    money %= 5
    x2 = money // 2
    x1 = money % 2
    return "10: {}, 5: {}, 2: {}, 1: {}".format(x10, x5, x2, x1)


if __name__ == "__main__":
    print(coin(28))
    print(change_coin(28))
    print(dyna_coin(28))
    print(change_dyna_coin(28))
