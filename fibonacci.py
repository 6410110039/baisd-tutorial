import timeit


def fibo(n):
    if n < 2:
        return n
    return fibo(n - 1) + fibo(n - 2)


def dyna_fibo(n):
    f0 = 0
    f1 = 1
    f2 = 1

    for i in range(2, n + 1):
        f2 = f0 + f1
        f0 = f1
        f1 = f2
    return f2


if __name__ == "__main__":

    print(
        "fibo",
        fibo(10),
        timeit.timeit(
            "fibo(10)",
            globals=globals(),
        ),
    )
    print("dyna_fibo", dyna_fibo(10), timeit.timeit("dyna_fibo(10)", globals=globals()))
