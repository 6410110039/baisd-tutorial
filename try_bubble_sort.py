def bubble_sort(data: list) -> list:
    sorted_data = data.copy()
    for i in range(len(data) - 1, 0, -1):
        for j in range(i):
            if sorted_data[j] > sorted_data[j + 1]:
                temp = sorted_data[j]
                sorted_data[j] = sorted_data[j + 1]
                sorted_data[j + 1] = temp

    return sorted_data


if __name__ == "__main__":
    data = list(map(int, input("enter 10 number: ").split()))

    sorted_data = bubble_sort(data)

    print(sorted_data)
# hello
