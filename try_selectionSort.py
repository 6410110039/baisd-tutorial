def selection_sort(data: list) -> list:
    sorted_data = data.copy()
    n = len(sorted_data)
    for i in range(n - 1, 0, -1):
        max_Index = 0

        for j in range(1, i + 1):
            if sorted_data[j] > sorted_data[max_Index]:
                max_Index = j

        if max_Index != i:
            temp = sorted_data[i]
            sorted_data[i] = sorted_data[max_Index]
            sorted_data[max_Index] = temp

    return sorted_data


if __name__ == "__main__":
    data = list(map(int, input("enter 5 number: ").split()))
    sorted_data = selection_sort(data)
    print(sorted_data)
